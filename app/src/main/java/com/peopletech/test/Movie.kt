package com.peopletech.test

data class Movie( var iron:Int,var irontext:String,var india:String,var film:String,var description:String,
                  var video:String,var casting:String,var rating:String,var photo:String,var star:Int)