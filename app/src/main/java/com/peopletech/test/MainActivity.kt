package com.peopletech.test

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.LinearLayout


import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*

class MainActivity : AppCompatActivity() {

   // var toolbar : Toolbar? = null;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
         //  var toolbar =findViewById<Toolbar>(R.id.toolbar)
           setSupportActionBar(toolbar)


        val list=ArrayList<Movie>()

        list.add(Movie(R.id.iron,"Yuvraj","India","Film","Yuvraj Singh is a left-handed batsman who also bowls left-arm orthodox spin, a flamboyant and fearless cricketer.\n"+
              "A better player of fast bowling.",
            "Videography","Casting","Rating","Photography",R.id.star
        ))
        list.add(Movie(R.id.iron,"Yuvraj","India","Film","Yuvraj Singh is a left-handed batsman who also bowls left-arm orthodox spin, a flamboyant and fearless cricketer.\n"+
                "A better player of fast bowling.",
            "Videography","Casting","Rating","Photography",R.id.star
        ))
        list.add(Movie(R.id.iron,"Yuvraj","India","Film","Yuvraj Singh is a left-handed batsman who also bowls left-arm orthodox spin, a flamboyant and fearless cricketer.\n"+
                "A better player of fast bowling.",
            "Videography","Casting","Rating","Photography",R.id.star
        ))


        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager=LinearLayoutManager(this,LinearLayout.VERTICAL,false)


        var Adapter1=Adapter(list)

        recyclerView.adapter=Adapter1


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.values,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        var id=item!!.itemId

        if (id==R.id.add){
            return true
        }else{
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}

